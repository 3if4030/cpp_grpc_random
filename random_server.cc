#include <iostream>
#include <string>
#include <memory>

#include <cstdlib>

#include <grpcpp/grpcpp.h>

#include "random.pb.h"
#include "random.grpc.pb.h"

using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::Status;

namespace if4030 {
namespace grpc {

class RandomServer final : public RandomService::Service {
public:
    RandomServer() : min_( 1 ), max_( 100 ) {}
    
    Status SetBounds( ServerContext * context,
                      const SetBoundsRequest *  request,
                      SetBoundsResponse  * response ) {
        if( request->min() <= request->max() ) {
            min_ = request->min();
            max_ = request->max();
        }
        return Status::OK;
    }
    
    Status NextRandom( ServerContext * context,
                       const NextRandomRequest * request,
                       NextRandomResponse * response ) {
        response->set_random( min_ +  rand() % ( max_ - min_ + 1 ));
        return Status::OK;
    }

private:
    int min_;
    int max_;
};

}
}

int main() {
    if4030::grpc::RandomServer random_server;
    
    std::string server_address( "0.0.0.0:50051" );
    ServerBuilder builder;
    builder.AddListeningPort( server_address, grpc::InsecureServerCredentials() );
    builder.RegisterService( &random_server );
    std::unique_ptr< Server > server( builder.BuildAndStart() );
    std::cout << "Server listening on " << server_address << std::endl;
    server->Wait();
    
    return 0;
}
