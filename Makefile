.PHONY: all clean

PROGS = random_server random_client

PROTO_SRC = random.pb.cc
PROTO_H = random.pb.h
GRPC_SRC = random.grpc.pb.cc
GRPC_H = random.grpc.pb.h
RANDOM_SRC = random_server.cc random_client.cc

SRC = $(PROTO_SRC) $(GRPC_SRC) $(RANDOM_SRC)

OBJS = $(SRC:%.cc=%.o)

CPPFLAGS += -I$(PWD)/.local/include
CXXFLAGS += -std=c++11
LDFLAGS += -L$(PWD)/.local/lib
LIBS = -lgrpc++_reflection -lgrpc++ -lprotobuf -lgrpc -lssl -lcrypto -lz -lcares -lnsl -lre2 \
       -labsl_status -labsl_cord -labsl_hash -labsl_bad_variant_access -labsl_city -labsl_raw_hash_set \
       -labsl_bad_optional_access -labsl_hashtablez_sampler -labsl_exponential_biased -lgpr \
       -labsl_synchronization -labsl_stacktrace -labsl_symbolize -labsl_debugging_internal \
       -labsl_demangle_internal -labsl_graphcycles_internal -labsl_time -labsl_civil_time \
       -labsl_time_zone -labsl_malloc_internal -labsl_str_format_internal -labsl_strings \
       -labsl_strings_internal -labsl_int128 -labsl_throw_delegate -labsl_base \
       -labsl_raw_logging_internal -labsl_dynamic_annotations -lpthread -labsl_log_severity \
       -labsl_spinlock_wait /usr/lib/x86_64-linux-gnu/librt.so -laddress_sorting -lupb \
       -ldl -lrt -lm -lpthread

PROTOC = $(PWD)/.local/bin/protoc

all: $(PROGS)

random_server: random_server.o random.grpc.pb.o random.pb.o
	$(CXX) $(CXXFLAGS) $(LDFLAGS) -o $@ $^ $(LIBS)

random_client: random_client.o random.grpc.pb.o random.pb.o
	$(CXX) $(CXXFLAGS) $(LDFLAGS) -o $@ $^ $(LIBS)

$(PROTO_SRC) $(PROTO_H): random.proto
	$(PROTOC) --cpp_out=. random.proto

$(GRPC_SRC) $(GRPC_H): random.proto
	$(PROTOC) --grpc_out=. --plugin=protoc-gen-grpc=$(PWD)/.local/bin/grpc_cpp_plugin random.proto

%.o: %.cc $(PROTO_H) $(GRPC_H)
	$(CXX) -c $(CXXFLAGS) $(CPPFLAGS) -o $@ $<

clean:
	rm -f $(PROGS) $(OBJS) $(PROTO_SRC) $(GRPC_SRC) $(PROTO_H) $(GRPC_H)
